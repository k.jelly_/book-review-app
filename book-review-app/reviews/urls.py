
# from django.contrib import admin
# from django.urls import path, include, reverse_lazy
# from django.views.generic.base import RedirectView

# urlpatterns = [
#     path('admin/', admin.site.urls),
#     path("reviews/", include("reviews.urls")),
#     path("", RedirectView.as_view(url=reverse_lazy("reviews_list")))
# ]

from django.urls import path

from reviews.views import list_reviews, create_review, review_detail

urlpatterns = [
    path("", list_reviews, name="reviews_list"),
    path("new/", create_review, name="create_review"),
    path("<int:id>/", review_detail, name="review_detail"),
]



